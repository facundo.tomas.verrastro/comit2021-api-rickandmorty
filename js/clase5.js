function domicilioEC5(parametro1, parametro2, parametro3) {
	let pais = parametro1 || "Estados Unidos";
	let provincia = parametro2 || "Los Angeles";
	let localidad = parametro3 || "Beberly Hills"
	let domicilio = "Su domicilio es: " + pais + " " + provincia + " " + localidad
	return domicilio;
}

function domicilioEC6(pais = "Estados Unidos", provincia = "Los Angeles", localidad = "Beberly Hills") {
	let domicilio = `Su domicilio es: ${pais} ${provincia} ${localidad}`
	return domicilio;
}

function nombreCompleto(nombre, apellido) {
	return `Su nombre es ${nombre} ${apellido}`;
}

function datosCompletos(nombre, apellido, pais, provincia, localidad) {
	return `${nombreCompleto(nombre,apellido)} y ${domicilioEC6(pais,provincia,localidad)}`
}

function cambiarTitulo(nuevoTitulo = "Titulo Vacio") {
	let titulo = document.getElementById("titulo");
	titulo.innerText = nuevoTitulo;
}
//button to top
//Get the button
var mybutton = document.getElementById("myBtn");
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
	scrollFunction()
};

function scrollFunction() {
	if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
		mybutton.style.display = "block";
	} else {
		mybutton.style.display = "none";
	}
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
	document.body.scrollTop = 0;
	document.documentElement.scrollTop = 0;
}
//validacion del form
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
	'use strict'

	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	var forms = document.querySelectorAll('.needs-validation')

	// Loop over them and prevent submission
	Array.prototype.slice.call(forms)
		.forEach(function (form) {
			form.addEventListener('submit', function (event) {
				if (!form.checkValidity()) {
					event.preventDefault()
					event.stopPropagation()
				}

				form.classList.add('was-validated')
			}, false)
		})
})()

//Cuerpo Modal
//personajes 
let btnPersonajes = document.getElementById("btnPersonajes")
btnPersonajes.addEventListener("click", () => {
	let bodyPersonajes = document.getElementById("personajesBody");
	
	//funcion dada en clase
	let character;
	let xhttp;
	let htmlString="<ul>";
	xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			character = this;
			let results = JSON.parse(character.responseText).results;
			for(personaje of results){
				let {name,status,species, type, gender, image} = personaje;
				htmlString+= `<li><b>Nombre: </b>${name}<br><b>Estado: </b>${status}<br><b>Especie: </b>${species}<br><b> Genero: </b>${gender} <br><img src="${image}">`
			}
			htmlString+=`</li>`
			bodyPersonajes.innerHTML=htmlString;
		}
	}

	xhttp.open("GET","https://rickandmortyapi.com/api/character/?page=1",true)
	xhttp.send();

})
//Lugares
let btnLugares = document.getElementById("btnLugares")
btnLugares.addEventListener("click", () => {
	let bodyLugares = document.getElementById("lugaresBody");
	
	let places;
	let xhttp;
	let htmlString="<ul>";
	xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			places = this;
			let results = JSON.parse(places.responseText).results;
			for(lugar of results){
				let {name,type,dimension,url,created} = lugar;
				htmlString+= `<li><b>Nombre:</b> ${name}<br><b>Tipo: </b>${type}<br><b>Dimension: </b>${dimension} <br><b>URL: </b>${url}<br><b>Fecha de Creacion en la API:</b> ${created}`
			}
			htmlString+=`</li>`
			bodyLugares.innerHTML=htmlString;
		}
	}

	xhttp.open("GET","https://rickandmortyapi.com/api/location/?page=1",true)
	xhttp.send();
})
//Episodios
let btnEpisodios = document.getElementById("btnEpisodios")
btnEpisodios.addEventListener("click", () => {
	let episodiosBody = document.getElementById("episodiosBody");
	
	let episodes;
	let xhttp;
	let htmlString="<ul>";
	xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			episodes = this;
			let results = JSON.parse(episodes.responseText).results;
			for(episodio of results){
				let {name,air_date,episode,url,created} = episodio;
				htmlString+= `<li><b>Nombre: </b>${name}<br><b>Fecha de Estreno: </b>${air_date}<br><b>Temporada: </b>${episode} <br><b>URL: </b>${url} <br><b>Fecha de Creacion en la API:</b>${created} `
			}
			htmlString+=`</li>`
			episodiosBody.innerHTML=htmlString;
		}
	}

	xhttp.open("GET","https://rickandmortyapi.com/api/episode/?page=1",true)
	xhttp.send();
})

//callbacks
function suma(num1, num2) {
	return num1 + num2;
}

function conectar(num1, num2, callback) {
	return callback(num1, num2)
}
console.log(conectar(5, 10, suma))

// var character;
// function loadCharacter(url){
// 	var xhttp;
// 	xhttp = new XMLHttpRequest();
// 	xhttp.onreadystatechange=function(){
// 		if(this.readyState==4 && this.status==200){
// 			character = this;
// 			console.log(JSON.parse(character.responseText));
// 		}
// 	}
// 	xhttp.open("GET",url,true)
// 	xhttp.send();
// }